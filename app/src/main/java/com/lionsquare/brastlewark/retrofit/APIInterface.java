package com.lionsquare.brastlewark.retrofit;




import com.lionsquare.brastlewark.retrofit.pojo.BrastlewarkPojo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface APIInterface {


    @GET("master/data.json")
    Call<BrastlewarkPojo> getGnomo();


}
