package com.lionsquare.brastlewark.retrofit.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BrastlewarkPojo {

    @SerializedName("Brastlewark")
    @Expose
    private List<Brastlewark> brastlewark = null;

    public List<Brastlewark> getBrastlewark() {
        return brastlewark;
    }

    public void setBrastlewark(List<Brastlewark> brastlewark) {
        this.brastlewark = brastlewark;
    }
}
