package com.lionsquare.brastlewark.di.commponent;


import com.lionsquare.brastlewark.di.module.AppModule;
import com.lionsquare.brastlewark.di.module.RetrofitModule;
import com.lionsquare.brastlewark.iu.BaseFragment;
import com.lionsquare.brastlewark.iu.main.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, RetrofitModule.class})
public interface AppComponent {

    void inject(MainActivity target);

    void inject(BaseFragment baseFragment);
}
