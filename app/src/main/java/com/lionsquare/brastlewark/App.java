package com.lionsquare.brastlewark;

import android.app.Application;

import com.lionsquare.brastlewark.di.commponent.AppComponent;
import com.lionsquare.brastlewark.di.commponent.DaggerAppComponent;
import com.lionsquare.brastlewark.di.module.AppModule;

public class App extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();

    }

    public AppComponent getAppComponent() {
        return appComponent;
    }


}
