package com.lionsquare.brastlewark.iu

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.util.Pair
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.lionsquare.brastlewark.R
import com.lionsquare.brastlewark.bean.BeanSection

abstract class BaseActivity : AppCompatActivity(), BaseFragment.SectionFragment {



    protected var sectionToolbar: Toolbar? = null
    protected var beanSection: BeanSection? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun updateSectionToolbar(section: BeanSection, sectionToolbar: Toolbar) {
        this.sectionToolbar = sectionToolbar
        this.beanSection = section
        setSupportActionBar(sectionToolbar)
        sectionToolbar.setTitle(beanSection!!.sectionNameId)
        this.sectionToolbar = sectionToolbar
        this.beanSection = beanSection

        sectionToolbar.setTitle(beanSection!!.sectionNameId)
        sectionToolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))

        setupToolbar(sectionToolbar, beanSection!!.HomeAsUpEnabled)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (beanSection!!.sectionColorPrimaryDarkId == 0) {
                window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
            } else {
                window.statusBarColor = ContextCompat.getColor(this, beanSection!!.sectionColorPrimaryDarkId)
            }
        }

    }

    override fun updateSectionToolbarTransparent(section: BeanSection, sectionToolbar: Toolbar) {
        this.sectionToolbar = sectionToolbar
        this.beanSection = section
        setSupportActionBar(sectionToolbar)
        sectionToolbar.setTitle(beanSection!!.sectionNameId)
        this.sectionToolbar = sectionToolbar
        this.beanSection = beanSection

        sectionToolbar.setTitle(beanSection!!.sectionNameId)
        sectionToolbar.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))

        setupToolbar(sectionToolbar, beanSection!!.HomeAsUpEnabled)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (beanSection!!.sectionColorPrimaryDarkId == 0) {
                window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
            } else {
                window.statusBarColor = ContextCompat.getColor(this, beanSection!!.sectionColorPrimaryDarkId)
            }
        }
    }


    /**
     * Method used to set the toolbar options and behaviours
     *
     * @param sectionToolbar the toolbar
     */
    open fun setupToolbar(sectionToolbar: Toolbar, HomeAsUpEnabled: Boolean) {
        //Empty implementation
    }



}
