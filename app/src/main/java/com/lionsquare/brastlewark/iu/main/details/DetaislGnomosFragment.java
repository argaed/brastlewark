package com.lionsquare.brastlewark.iu.main.details;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.transition.TransitionInflater;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.tabs.TabLayout;
import com.lionsquare.brastlewark.R;
import com.lionsquare.brastlewark.adapter.FragmentPagerAdapter;
import com.lionsquare.brastlewark.bean.BeanSection;
import com.lionsquare.brastlewark.databinding.FragmentDetaislGnomosBinding;
import com.lionsquare.brastlewark.iu.BaseFragment;
import com.lionsquare.brastlewark.iu.main.gnomos.GnomosFragment;
import com.lionsquare.brastlewark.retrofit.pojo.Brastlewark;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetaislGnomosFragment extends BaseFragment {


    public static final String TAG = "DetaislGnomosFragment";

    public static DetaislGnomosFragment newInstance(Bundle arguments) {
        DetaislGnomosFragment f = new DetaislGnomosFragment();
        if (arguments != null) {
            f.setArguments(arguments);
        }
        return f;
    }

    private FragmentDetaislGnomosBinding binding;

    private TabLayout tabLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSharedElementEnterTransition(TransitionInflater.from(mContext).inflateTransition(android.R.transition.move));
        beanSection = new BeanSection();
        beanSection.setSectionNameId(R.string.details);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_detaisl_gnomos, container, false);
        View view = binding.getRoot();


        Bundle bundle = getArguments();
        FragmentPagerAdapter adapter = null;
        if (bundle != null) {
            Brastlewark brastlewark = (Brastlewark) bundle.getSerializable("braData");
            sectionFragment.updateSectionToolbarTransparent(beanSection, binding.toolbar);
            binding.toolbar.setTitle(brastlewark.getName());
            Bundle args = new Bundle();
            args.putSerializable("braData", brastlewark);
            adapter = new FragmentPagerAdapter(getChildFragmentManager(), args);
            tabLayout = binding.tabs;
            tabLayout.addTab(tabLayout.newTab().setText("Profesiones"));
            tabLayout.addTab(tabLayout.newTab().setText("Amigos"));

            ViewPager viewPager = binding.vpFragment;

            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
            setProfile(brastlewark);
        } else {
            sectionFragment.updateSectionToolbarTransparent(beanSection, binding.toolbar);
        }


        return view;
    }

    private void setProfile(Brastlewark brastlewark) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.fitCenter();
        Glide.with(mContext).load(brastlewark.getThumbnail()).apply(requestOptions).into(binding.expandedImage);
    }


}
