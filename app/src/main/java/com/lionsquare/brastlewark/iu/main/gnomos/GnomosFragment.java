package com.lionsquare.brastlewark.iu.main.gnomos;


import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lionsquare.brastlewark.R;
import com.lionsquare.brastlewark.adapter.GnomoAdapeter;
import com.lionsquare.brastlewark.bean.BeanSection;
import com.lionsquare.brastlewark.databinding.FragmentGnomosBinding;
import com.lionsquare.brastlewark.iu.BaseFragment;
import com.lionsquare.brastlewark.iu.main.details.DetaislGnomosFragment;
import com.lionsquare.brastlewark.retrofit.pojo.Brastlewark;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class GnomosFragment extends BaseFragment implements GnomoAdapeter.OnClicklistener {


    public static final String TAG = "GnomosFragment";

    public static GnomosFragment newInstance(Bundle arguments) {
        GnomosFragment f = new GnomosFragment();
        if (arguments != null) {
            f.setArguments(arguments);
        }
        return f;
    }

    private RecyclerView rvGnomo;
    private GnomoAdapeter gnomoAdapeter;
    private GnomosViewModel gnomosViewModel;

    private SearchView searchView;

    private String mSearchString;

    public MenuItem searchMenuItem;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        beanSection = new BeanSection();
        beanSection.setSectionNameId(R.string.main);
        if (savedInstanceState != null) {
            mSearchString = savedInstanceState.getString("SEARCH_KEY");
        }

        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        FragmentGnomosBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_gnomos, container, false);


        View view = binding.getRoot();

        sectionFragment.updateSectionToolbar(beanSection, binding.toolbar);

        if (gnomoAdapeter == null) {
            gnomoAdapeter = new GnomoAdapeter(mContext, new ArrayList<Brastlewark>());
        }


        gnomosViewModel = ViewModelProviders.of(this).get(GnomosViewModel.class);
        binding.setLoginViewModel(gnomosViewModel);
        binding.setLifecycleOwner(this);


        gnomosViewModel.getGnomo(apiInterface).observe(this, new Observer<List<Brastlewark>>() {
            @Override
            public void onChanged(List<Brastlewark> brastlewarks) {
                gnomoAdapeter.setData(brastlewarks);
            }
        });


        rvGnomo = binding.rvGnomos;
        rvGnomo.setLayoutManager(new LinearLayoutManager(mContext));
        rvGnomo.hasFixedSize();
        rvGnomo.setAdapter(gnomoAdapeter);
        gnomoAdapeter.setOnClickListener(this);

        return view;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public void OnClick(@NotNull Brastlewark brastlewark, ImageView imageAvatar) {
        Pair<View, String> pair = new Pair<>((View) imageAvatar, ViewCompat.getTransitionName(imageAvatar));
        Bundle args = new Bundle();
        args.putSerializable("braData", brastlewark);
        sectionFragment.pushFragmentSharedElement(DetaislGnomosFragment.TAG, DetaislGnomosFragment.newInstance(args), pair);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mSearchString = searchView.getQuery().toString();
        outState.putString("SEARCH_KEY", mSearchString);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
         searchMenuItem = menu.findItem(R.id.action_search);


        SearchManager searchManager = (SearchManager) mContext.getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) searchMenuItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                gnomoAdapeter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                gnomoAdapeter.getFilter().filter(query);
                Log.e("item2", String.valueOf(gnomoAdapeter.getItemCount()));
                return false;
            }
        });

        //focus the SearchView
        if (mSearchString != null && !mSearchString.isEmpty()) {

            searchMenuItem.expandActionView();
            searchView.setQuery(mSearchString, false);
            searchView.clearFocus();
            Log.e("item", String.valueOf(gnomoAdapeter.getItemCount()));
        } else {

        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);

    }


}
