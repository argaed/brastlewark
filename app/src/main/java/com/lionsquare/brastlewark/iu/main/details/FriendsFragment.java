package com.lionsquare.brastlewark.iu.main.details;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lionsquare.brastlewark.R;
import com.lionsquare.brastlewark.adapter.DetailsAdapter;
import com.lionsquare.brastlewark.databinding.FragmentFriendsBinding;
import com.lionsquare.brastlewark.iu.BaseFragment;
import com.lionsquare.brastlewark.retrofit.pojo.Brastlewark;

/**
 * A simple {@link Fragment} subclass.
 */
public class FriendsFragment extends BaseFragment {


    public static FriendsFragment newInstance(Bundle arguments) {
        FriendsFragment f = new FriendsFragment();
        if (arguments != null) {
            f.setArguments(arguments);
        }
        return f;
    }

    private RecyclerView rvFriends;
    private DetailsAdapter detailsAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentFriendsBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_friends, container, false);
        rvFriends = binding.rvFiends;
        rvFriends.setLayoutManager(new LinearLayoutManager(mContext));
        rvFriends.hasFixedSize();


        Bundle bundle = getArguments();
        if (bundle != null) {
            Brastlewark brastlewark = (Brastlewark) bundle.getSerializable("braData");
            if (brastlewark.getFriends() != null) {
                if (brastlewark.getFriends().size() == 0) {
                    binding.tvNoData.setVisibility(View.VISIBLE);
                }
                detailsAdapter = new DetailsAdapter(mContext, brastlewark.getFriends(), 1);
                rvFriends.setAdapter(detailsAdapter);
            }


        }
        return binding.getRoot();
    }

}
