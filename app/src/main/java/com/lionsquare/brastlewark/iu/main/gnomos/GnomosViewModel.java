package com.lionsquare.brastlewark.iu.main.gnomos;

import android.app.Application;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.lionsquare.brastlewark.retrofit.APIInterface;
import com.lionsquare.brastlewark.retrofit.pojo.Brastlewark;
import com.lionsquare.brastlewark.retrofit.pojo.BrastlewarkPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GnomosViewModel extends AndroidViewModel {

    APIInterface apiInterface;

    public MutableLiveData<List<Brastlewark>> postList;
    public MutableLiveData<Integer> vProgress;
    public MutableLiveData<String> mSearchString;

    public GnomosViewModel(@NonNull Application application) {
        super(application);
        vProgress = new MutableLiveData<>();
        mSearchString = new MutableLiveData<>();
    }

    public MutableLiveData<Integer> getBusy() {
        if (vProgress == null) {
            vProgress = new MutableLiveData<>();
            vProgress.setValue(0);
        }
        return vProgress;
    }


    MutableLiveData<List<Brastlewark>> getGnomo(APIInterface apiInterface) {
        this.apiInterface = apiInterface;
        if (postList == null) {
            postList = new MutableLiveData<>();
            postList = getList();
        }


        return postList;
    }

    private MutableLiveData<List<Brastlewark>> getList() {
        getBusy().setValue(0);
        apiInterface.getGnomo().enqueue(new Callback<BrastlewarkPojo>() {
            @Override
            public void onResponse(Call<BrastlewarkPojo> call, Response<BrastlewarkPojo> response) {
                postList.setValue(response.body().getBrastlewark());
                getBusy().setValue(8);
            }

            @Override
            public void onFailure(Call<BrastlewarkPojo> call, Throwable t) {
                getBusy().setValue(8);

            }
        });

        return postList;
    }

}
