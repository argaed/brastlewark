package com.lionsquare.brastlewark.iu.main.details;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lionsquare.brastlewark.R;
import com.lionsquare.brastlewark.adapter.DetailsAdapter;
import com.lionsquare.brastlewark.adapter.FragmentPagerAdapter;
import com.lionsquare.brastlewark.bean.BeanSection;
import com.lionsquare.brastlewark.databinding.FragmentProfessionsBinding;
import com.lionsquare.brastlewark.iu.BaseFragment;
import com.lionsquare.brastlewark.retrofit.pojo.Brastlewark;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfessionsFragment extends BaseFragment {


    public static ProfessionsFragment newInstance(Bundle arguments) {
        ProfessionsFragment f = new ProfessionsFragment();
        if (arguments != null) {
            f.setArguments(arguments);
        }
        return f;
    }

    private RecyclerView rvProf;
    private DetailsAdapter detailsAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentProfessionsBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_professions, container, false);
        rvProf = binding.rvProfession;
        rvProf.setLayoutManager(new LinearLayoutManager(mContext));
        rvProf.hasFixedSize();


        Bundle bundle = getArguments();
        if (bundle != null) {
            Brastlewark brastlewark = (Brastlewark) bundle.getSerializable("braData");
            if (brastlewark.getProfessions() != null) {
                if (brastlewark.getProfessions().size() == 0) {
                    binding.tvNoData.setVisibility(View.VISIBLE);
                }
                detailsAdapter = new DetailsAdapter(mContext, brastlewark.getProfessions(), 0);
                rvProf.setAdapter(detailsAdapter);
            }


        }


        return binding.getRoot();
    }

}
