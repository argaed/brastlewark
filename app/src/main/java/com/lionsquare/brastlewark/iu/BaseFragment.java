package com.lionsquare.brastlewark.iu;

import android.content.Context;
import android.util.Pair;
import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.lionsquare.brastlewark.App;
import com.lionsquare.brastlewark.bean.BeanSection;
import com.lionsquare.brastlewark.iu.main.MainActivity;
import com.lionsquare.brastlewark.retrofit.APIInterface;

import javax.inject.Inject;

public class BaseFragment extends Fragment {

    protected SectionFragment sectionFragment;
    protected Toolbar toolbar;
    protected BeanSection beanSection;

    @Inject
    public Context mContext;
    @Inject
    public APIInterface apiInterface;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((App) getActivity().getApplication()).getAppComponent().inject(this);

        if (context instanceof MainActivity) {
            sectionFragment = (SectionFragment) context;
        }
    }

    public interface SectionFragment {
        void updateSectionToolbar(BeanSection section, Toolbar sectionToolbar);

        void updateSectionToolbarTransparent(BeanSection section, Toolbar sectionToolbar);

        void pushFragment(String tag, Fragment fragment);

        void pushFragmentSharedElement(String tag, Fragment fragment, Pair<View, String> pair);
    }


}
