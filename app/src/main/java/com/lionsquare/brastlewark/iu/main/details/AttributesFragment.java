package com.lionsquare.brastlewark.iu.main.details;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lionsquare.brastlewark.R;
import com.lionsquare.brastlewark.adapter.DetailsAdapter;
import com.lionsquare.brastlewark.iu.BaseFragment;
import com.lionsquare.brastlewark.retrofit.pojo.Brastlewark;

/**
 * A simple {@link Fragment} subclass.
 */
public class AttributesFragment extends BaseFragment {


    public static AttributesFragment newInstance(Bundle arguments) {
        AttributesFragment f = new AttributesFragment();
        if (arguments != null) {
            f.setArguments(arguments);
        }
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attributes, container, false);
        TextView tvEda = view.findViewById(R.id.tv_edad);
        TextView tvPeso = view.findViewById(R.id.tv_peso);
        TextView tvAltura = view.findViewById(R.id.tv_altura);
        TextView tvCabelloColor = view.findViewById(R.id.tv_color_cabello);
        Bundle bundle = getArguments();
        if (bundle != null) {
            Brastlewark brastlewark = (Brastlewark) bundle.getSerializable("braData");
            tvEda.setText(brastlewark.getAge().toString());
            tvPeso.setText(brastlewark.getWeight().toString());
            tvAltura.setText(brastlewark.getHeight().toString());
            tvCabelloColor.setText(brastlewark.getHairColor());


        }
        return view;
    }

}
