package com.lionsquare.brastlewark.iu.main;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.lionsquare.brastlewark.App;
import com.lionsquare.brastlewark.R;
import com.lionsquare.brastlewark.iu.BaseActivity;
import com.lionsquare.brastlewark.iu.main.gnomos.GnomosFragment;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

public class MainActivity extends BaseActivity {

    @Inject
    Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((App) getApplication()).getAppComponent().inject(this);

        if (savedInstanceState == null) {
            FragmentManager fm = getSupportFragmentManager();
            GnomosFragment fragment = GnomosFragment.newInstance(null);
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fragment, fragment, GnomosFragment.TAG);
            ft.commit();
        }


    }

    @Override
    public void setupToolbar(@NotNull Toolbar sectionToolbar, boolean HomeAsUpEnabled) {
        super.setupToolbar(sectionToolbar, HomeAsUpEnabled);

    }

    @Override
    public void pushFragment(String tagFragment, Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        fm.executePendingTransactions();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment, fragment, tagFragment);
        ft.addToBackStack(tagFragment);
        ft.commit();
    }

    @Override
    public void pushFragmentSharedElement(String tag, Fragment fragment, Pair<View, String> pair) {
        Log.e("element", pair.second);
        ImageView imageView = (ImageView) pair.first;

        getSupportFragmentManager()
                .beginTransaction()
                .addSharedElement(imageView, ViewCompat.getTransitionName(imageView))
                .addToBackStack(tag)
                .replace(R.id.fragment, fragment).addToBackStack(tag)
                .commit();

    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 0) {

            super.onBackPressed();



        } else {
            getSupportFragmentManager().popBackStack();
        }
    }
}
