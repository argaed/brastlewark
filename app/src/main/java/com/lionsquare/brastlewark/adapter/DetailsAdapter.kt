package com.lionsquare.brastlewark.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lionsquare.brastlewark.R

class DetailsAdapter(private val mContext: Context, private var data: List<String>, private val type: Int) : RecyclerView.Adapter<DetailsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)
        val view = layoutInflater.inflate(R.layout.item_professions, parent, false)

        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.cvRoot.setOnClickListener { }
        holder.txtName.text = data[position]
        if (type == 0) {
            Glide.with(mContext).load(R.drawable.ic_profession).into(holder.imageView)
        } else {
            Glide.with(mContext).load(R.drawable.ic_friend).into(holder.imageView)
        }


    }

    override fun getItemCount(): Int {


        return data.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var cvRoot: CardView = itemView.findViewById(R.id.cv_root)
        internal var txtName: TextView = itemView.findViewById(R.id.txtName)
        internal var imageView: ImageView = itemView.findViewById(R.id.img_avatar)


        init {


        }
    }

    fun setData(data: List<String>) {
        this.data = data
        notifyDataSetChanged()
    }


}
