package com.lionsquare.brastlewark.adapter;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.lionsquare.brastlewark.iu.main.details.AttributesFragment;
import com.lionsquare.brastlewark.iu.main.details.FriendsFragment;
import com.lionsquare.brastlewark.iu.main.details.ProfessionsFragment;

public class FragmentPagerAdapter extends androidx.fragment.app.FragmentPagerAdapter {

    private Bundle bundle;

    public FragmentPagerAdapter(FragmentManager fm, Bundle bundle) {
        super(fm);
        this.bundle = bundle;
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return AttributesFragment.newInstance(bundle);
        } else if (position == 1) {
            return ProfessionsFragment.newInstance(bundle);
        } else if (position == 2) {
            return FriendsFragment.newInstance(bundle);
        } else {
            return new FriendsFragment();
        }
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 3;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Detalles";
            case 1:
                return "Profesiones";
            case 2:
                return "Amigos";

            default:
                return null;
        }
    }

}