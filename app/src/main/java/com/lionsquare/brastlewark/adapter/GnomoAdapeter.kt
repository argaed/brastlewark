package com.lionsquare.brastlewark.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.lionsquare.brastlewark.R
import com.lionsquare.brastlewark.retrofit.pojo.Brastlewark
import java.util.ArrayList

class GnomoAdapeter(private val mContext: Context, private var data: List<Brastlewark>) : RecyclerView.Adapter<GnomoAdapeter.ViewHolder>()
        , Filterable {
    private lateinit var dataFilter: List<Brastlewark>

    init {
          dataFilter=data
    }

    lateinit var onClicklistener: OnClicklistener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(mContext)
        val view = layoutInflater.inflate(R.layout.item_gnomos, parent, false)

        return ViewHolder(view)
    }

    @SuppressLint("CheckResult", "SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.cvRoot.setOnClickListener { onClicklistener.OnClick(dataFilter[position], holder.imageView) }
        holder.txtName.text = "Nombre :" + dataFilter[position].name

        holder.txtAge.text = "Edad :" + dataFilter[position].age.toString()
        val options = RequestOptions()
        //options.placeholder()
        options.error(mContext.getDrawable(R.drawable.ic_image_broken))

        Glide.with(mContext).load(dataFilter[position].thumbnail).apply(options)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                    ): Boolean {
                        holder.imm_progress.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataFilterSource: DataSource?,
                            isFirstResource: Boolean
                    ): Boolean {
                        holder.imm_progress.visibility = View.GONE

                        return false
                    }
                }).into(holder.imageView)

    }

    override fun getItemCount(): Int {


        return dataFilter.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var cvRoot: CardView = itemView.findViewById(R.id.cv_root)
        internal var txtName: TextView = itemView.findViewById(R.id.txtName)
        internal var txtAge: TextView = itemView.findViewById(R.id.txtAge)
        internal var imageView: ImageView = itemView.findViewById(R.id.img_avatar)
        internal val imm_progress: ProgressBar = itemView.findViewById(R.id.imm_progress) as ProgressBar

        init {

            imm_progress.visibility = View.VISIBLE

        }
    }




    fun setData(dataFilter: List<Brastlewark>) {
        this.dataFilter = dataFilter
        this.data = dataFilter
        notifyDataSetChanged()
    }

    fun setOnClickListener(onClicklistener: OnClicklistener) {
        this.onClicklistener = onClicklistener
    }

    interface OnClicklistener {
        fun OnClick(brastlewark: Brastlewark, imageAvatar: ImageView)
    }

    override fun getFilter(): Filter {

        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    dataFilter = data
                } else {
                    val filteredList = ArrayList<Brastlewark>()

                    for (row in data) {
                        if (row.name.toLowerCase().contains(charString.toLowerCase()) || row.age.toString().contains(charSequence)) {

                            filteredList.add(row)
                        }
                    }

                    dataFilter = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = dataFilter
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                dataFilter = filterResults.values as ArrayList<Brastlewark>
                notifyDataSetChanged()
            }
        }
    }
}
